
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Login from './Component/Login';
import ProfileScreen from './Screen/ProfileScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';

class App extends Component {
  render() {
    const Stack = createStackNavigator();
    return (
      <View style={{ flex: 1 }}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Login">
            <Stack.Screen options={{ headerShown: false }} name="Login" component={Login} />
            <Stack.Screen name="Profile" component={ProfileScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </View>

    )
  }
}
export default App;
