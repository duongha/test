import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';



const Login = ({ navigation }) => {
    return (
        <View style={{ flex: 1, backgroundColor: 'green' }}>


            <View style={{ flex: 1, }}>
                <Image
                    style={{ height: 30, width: 30, margin: 20 }}
                    source={{
                        uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTOwKG3IUUOZmx4MPAxeJsIj_foelKpbqnJhg&usqp=CAU',
                    }}
                />
                <Image
                    style={{ height: 150, width: 150, borderRadius: 75, alignSelf: 'center' }}
                    source={{
                        uri: 'https://reactnative.dev/img/tiny_logo.png',
                    }}
                />


            </View>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', textAlign: 'center', fontSize: 12, fontStyle: 'italic' }}>
                    Vui lòng nhập số điện thoại
                </Text>
                <TextInput
                    style={{ height: 50, width: 220, paddingLeft: 6, textAlign: 'center', opacity: 0.6 }}
                    selectionColor='#428AF8'
                    underlineColorAndroid='#fff'
                    placeholderTextColor="white"
                    placeholder="Số điện thoại"
                />

                <TouchableOpacity
                    style={{
                        width: 220,
                        height: 40,
                        backgroundColor: 'green',
                        borderWidth: 1,
                        borderColor: 'white',
                        borderRadius: 50,
                        margin: 15
                    }} onPress={() => {
                        navigation.navigate('Profile')
                    }}>
                    <Text style={{
                        color: 'white',
                        fontSize: 18,
                        textAlign: 'center',
                        padding: 7,
                        fontWeight: '600'
                    }}>ĐĂNG NHẬP</Text>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ color: '#fff', marginRight: 7 }}>Bạn có tài khoản chưa?</Text>
                    <Text style={{ color: 'yellow', fontWeight: 'bold' }}>Đăng ký</Text>
                </View>

            </View>
            <View style={{
                flex: 1, flexDirection: 'row',
                alignItems: 'flex-end',
                justifyContent: 'center',
                paddingBottom: 30
            }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                    <Image style={{ height: 30, width: 30, marginRight: 10 }}
                        source={{
                            uri: 'https://w0.pngwave.com/png/62/562/computer-icons-warranty-png-clip-art.png',
                        }}></Image>
                    <Text style={{ textAlign: 'center', color: 'white' }}>Gọi chăm sóc khách hàng</Text>
                </View>
            </View>
        </View >
    );

}


export default Login;