import React, { Component } from 'react';
import { View, Text, ActivityIndicator, FlatList, Alert, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';


class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = { isLoading: true }
    }

    componentDidMount() {
        return fetch('https://reactnative.dev/movies.json')
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({
                    isLoading: false,
                    dataSource: responseJson.movies,
                }, function () {

                });

            })
            .catch((error) => {
                console.error(error);
            });

    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{
                    flex: 1, padding: 50, flexDirection: "row",
                    justifyContent: "space-around",
                    padding: 10
                }}>
                    <ActivityIndicator />

                    <ActivityIndicator size="small" color="#0000ff" />

                </View>
            )
        }

        return (
            <View style={{ flex: 1, paddingTop: 20 }}>

                <FlatList
                    data={this.state.dataSource}

                    renderItem={({ item }) => <TouchableOpacity style={{
                        alignSelf: 'center',
                        width: '80%',
                        height: 100,
                        marginTop: 20,
                        borderColor: 'red',
                        borderWidth: 1,
                        justifyContent: 'center'
                    }} onPress={() => {
                        if (item.id % 2 == 0) {
                            alert(item.title)

                        } else {
                            alert(item.id)

                        }
                    }}>
                        <Image
                            style={{ height: 90, width: '100%', alignSelf: 'center' }}
                            source={{
                                uri: 'https://picsum.photos/200/300',
                            }}
                        />
                        <Text style={{ color: 'red', textAlign: 'center' }}>{item.title}, {item.releaseYear}</Text></TouchableOpacity>}
                    keyExtractor={({ id }, index) => id}
                />


            </View>
        );
    }

}
export default Profile;